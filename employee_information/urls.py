from . import views
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.views.generic.base import RedirectView

urlpatterns = [
    path('redirect-admin', RedirectView.as_view(url="/admin"),name="redirect-admin"),
    path('', views.home, name="home-page"),
    path('login', auth_views.LoginView.as_view(template_name = 'employee_information/login.html',redirect_authenticated_user=True), name="login"),
    path('userlogin', views.login_user, name="login-user"),
    path('logout', views.logoutuser, name="logout"),
    path('about', views.about, name="about-page"),
    path('recruiters', views.recruiters, name="department-page"),
    path('manage_recruiters', views.manage_recruiters, name="manage_departments-page"),
    path('save_recruiters', views.save_recruiter, name="save-department-page"),
    path('delete_recruiters', views.delete_recruiter, name="delete-department"),
    path('employers', views.employers, name="position-page"),
    path('manage_employers', views.manage_employers, name="manage_positions-page"),
    path('save_employers', views.save_employers, name="save-position-page"),
    path('delete_employers', views.delete_employers, name="delete-position"),
    path('members', views.members, name="employee-page"),
    path('manage_members', views.manage_members, name="manage_employees-page"),
    path('save_member', views.save_members, name="save-employee-page"),
    path('delete_member', views.delete_members, name="delete-employee"),
    path('view_member', views.view_member, name="view-employee-page"),
]