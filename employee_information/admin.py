from django.contrib import admin
from .models import Member,Recruiter,Employer,RecruiterType

# Register your models here.
admin.site.register(Member)
admin.site.register(Recruiter)
admin.site.register(Employer)
admin.site.register(RecruiterType)

