from datetime import datetime
from django.db import models
from django.utils import timezone


# Create your models here.

# recruiter information
class RecruiterType(models.Model):
    category = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.category


class Recruiter(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(default="no description",max_length=30)
    status = models.IntegerField(default=1)
    recruiterType = models.OneToOneField(RecruiterType, on_delete=models.CASCADE)
    recruitedBy=models.CharField(max_length=50,default="leave admin if recruiter is not sub recruiter")
    date_added = models.DateTimeField(default=timezone.now)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


# position is employer info
class Employer(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()
    status = models.IntegerField()
    county = models.CharField(blank=True, null=True,max_length=30)
    date_added = models.DateTimeField(default=timezone.now)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name




class Member(models.Model):
    code = models.CharField(max_length=100, blank=True)
    firstname = models.CharField(max_length=20)
    middlename = models.CharField(max_length=20 ,blank=True, null=True)
    lastname = models.CharField(max_length=20)
    gender = models.CharField(max_length=10,null=True,blank=True)
    dob = models.DateField(blank=True, null=True)
    contact = models.CharField(max_length=20)
    address = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    recruiter_id = models.ForeignKey(Recruiter, on_delete=models.CASCADE)
    employer_id = models.ForeignKey(Employer, on_delete=models.CASCADE)
    status = models.IntegerField()
    signature = models.ImageField(upload_to="images/",null=True,blank=True)
    cover = models.ImageField(upload_to='images/')
    date_added = models.DateTimeField(default=timezone.now)
    date_updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.firstname




















